import TestPage from './components/TestPage'
import LetuHeader from './components/LetuHeader'
import LetuFooter from './components/LetuFooter'

export default function App() {
    return (
        <>
            <LetuHeader />
            <TestPage />
            <LetuFooter /> 
        </>
    )
}
