import logoLetu from '../assets/logos/LETUShield/LETUShield-FllClrBlue.svg'
import logoACM from '../assets/logos/ACM-Standalone/ACM.svg'
import logoLetuFullCS from '../assets/logos/LETU/LETU-ComputerScience-FllClrBlue.svg'

export default function Letufooter() {
    return (
        <div className="bg-grey w-full p-3">
            <img src={logoLetu} className="h-24 inline-block" alt="LETU Logo" />
            <img src={logoACM} className="h-24 inline-block" alt="ACM Logo"/>
            <img src={logoLetuFullCS} className="h-24 float-right relative inline-block" alt="LeTourneau Computer Science logo"/>
        </div>
    )
}
