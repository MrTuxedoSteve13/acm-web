import logoLetu from '../assets/logos/LETUShield/LETUShield-FllClrBlue.svg'
import logoLetuFull from '../assets/logos/LETU/LETU-FllClrBlue.svg'
import logoLetuFullCS from '../assets/logos/LETU/LETU-ComputerScience-FllClrBlue.svg'
import logoACMFull from '../assets/logos/ACM/ACM.svg'
import logoACM from '../assets/logos/ACM-Standalone/ACM.svg'
import ColorTest from './TestColor'
import FontTest from './TestFont'

interface LogoProps {
    src: string
    alt: string
}
function Logo({ src, alt }: LogoProps) {
    return <img src={src} alt={alt} className="h-24 inline-block m-4" />
}

export default function TestPage() {
    return (
        <>
            <div>
                <Logo src={logoLetu} alt="LeTourneau Logo Shield" />
                <Logo src={logoLetuFull} alt="LeTourneau Logo" />
                <Logo
                    src={logoLetuFullCS}
                    alt="LeTourneau Computer Science logo"
                />
                <Logo src={logoACM} alt="ACM lgo Standalone" />
                <Logo src={logoACMFull} alt="ACM logo" />
            </div>
            <ColorTest />
            <FontTest text="LeTourneau ACM" />
            <FontTest text="I like to test" />
        </>
    )
}
